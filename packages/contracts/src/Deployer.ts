import { ethers, Contract, Signer, ContractFactory } from 'ethers'
import { exchangeByteCode, exchangeByteAbi, factoryAbi, factoryByteCode } from "./CONSTANTS";
import * as TOKEN_JSON from './compiled_contracts/Token.json'
import * as REBALANCE_JSON from './compiled_contracts/Rebalance.json'
import { BigNumberish } from 'ethers/utils';

export const TX_OVERRIDES = {
    gasLimit: 8000000,
    //  gasPrice: ethers.utils.parseUnits('0.0', 'gwei'),
};

export type Token = {
    name: string
    symbol: string,
    amount: number,
    depositAmount: number,
    valueInEther: number
}

export type RebalancePackage = {
    tokenExchanges: TokenExchange[]
    rebalanceAddress: String
}

export type TokenExchange = {
    tokenAddress: string,
    exchangeAddress: string
}

export const DEFAULT_TOKENS: Token[] = [
    {
        name: "MakerDAO",
        symbol: "MKR",
        amount: 50000,
        depositAmount: 200,
        valueInEther: 516
    },
    {
        name: "Basic Attention Token",
        symbol: "BAT",
        amount: 200000,
        depositAmount: 5000,
        valueInEther: 15
    },
    {
        name: "Dai",
        symbol: "DAI",
        amount: 100000,
        depositAmount: 25000,
        valueInEther: 140
    },
    {
        name: "Binance",
        symbol: "BNB",
        amount: 700000,
        depositAmount: 14000,
        valueInEther: 50
    },
    {
        name: "ChainLink",
        symbol: "LINK",
        amount: 19000000,
        depositAmount: 120000,
        valueInEther: 100
    }
]

export class Deployer {
    signer!: Signer
    EXCHANGE_CONTRACT!: Contract
    FACTORY_CONTRACT!: Contract
    REBALANCE_CONTRACT!: Contract
    TOKEN_FACTORY!: ContractFactory
    TOKENS!: Contract[]
    ready = false

    constructor(signer: Signer | any) {

        if (signer instanceof Signer) {
            // Our signer is coming from local RPC probably
            ethers.utils.defineReadOnly(this, 'signer', signer)
        } else {
            //Our singer is coming from Metamask
            const web3Provider = new ethers.providers.Web3Provider(signer)
            ethers.utils.defineReadOnly(this, 'signer', web3Provider.getSigner())
        }
    }

    async setup(tokens?: Token[]): Promise<RebalancePackage> {
        await this.exchangeFactory()
        await this.factoryFactory()
        await this.tokenFactory()
        await this.initializeFactory()
        if (!tokens) {
            tokens = DEFAULT_TOKENS
        }
        let exchanges: TokenExchange[] = []
        for (let i = 0; i < tokens.length; i++) {
            const token = tokens[i]
            exchanges.push(await this.tokenExchange(token.name, token.symbol, token.amount, token.depositAmount, token.valueInEther))
        }
        await this.deployRabalance(exchanges)
        return {
            tokenExchanges: exchanges,
            rebalanceAddress: this.REBALANCE_CONTRACT.address
        }
    }


    private async exchangeFactory(): Promise<Contract> {
        const exchangeContractFactory = new ethers.ContractFactory(exchangeByteAbi, exchangeByteCode, this.signer);
        this.EXCHANGE_CONTRACT = await exchangeContractFactory.deploy();
        return await this.EXCHANGE_CONTRACT.deployed();
    }

    private async factoryFactory(): Promise<Contract> {
        const factoryContractFactory = new ethers.ContractFactory(factoryAbi, factoryByteCode, this.signer);
        this.FACTORY_CONTRACT = await factoryContractFactory.deploy();
        return await this.FACTORY_CONTRACT.deployed();
    }
    private async tokenFactory(): Promise<ContractFactory> {
        this.TOKEN_FACTORY = new ethers.ContractFactory(TOKEN_JSON.abi, TOKEN_JSON.bytecode, this.signer);
        return this.TOKEN_FACTORY
    }

    private async deployToken(name: string, symbol: string, amount: number): Promise<Contract> {
        const tokenContract = await this.TOKEN_FACTORY.deploy(name, symbol, amount);
        return await tokenContract.deployed();
    }

    private async initializeFactory(): Promise<boolean> {
        const tx = await this.FACTORY_CONTRACT.initializeFactory(this.EXCHANGE_CONTRACT.address, TX_OVERRIDES);
        await tx.wait();
        this.ready = true
        return this.ready
    }

    private async createExchange(tokenAddress: string, tokenDepositAmount?: number, tokenValueInEther?: number): Promise<Contract> {
        const tx1 = await this.FACTORY_CONTRACT.createExchange(tokenAddress, TX_OVERRIDES);
        await tx1.wait()
        const tokenExchangeAddress = await this.FACTORY_CONTRACT.getExchange(tokenAddress);
        const tokenExchange = new ethers.Contract(tokenExchangeAddress, exchangeByteAbi, this.signer);
        const tokenContract = new ethers.Contract(tokenAddress, TOKEN_JSON.abi, this.signer)
        let tokenDepositAmountBN: BigNumberish
        // If no deposit amount added, deposit the full balance of this address
        tokenDepositAmountBN = (tokenDepositAmount) ? ethers.utils.parseEther(tokenDepositAmount.toString()) : await tokenContract.balanceOf(await this.signer.getAddress())
        let tokenValueInEtherBN: BigNumberish
        // If not value in ether is set, 1 token = 1 ether
        tokenValueInEtherBN = (tokenValueInEther) ? ethers.utils.parseEther(tokenValueInEther.toString()) : ethers.utils.parseEther('1')
        const tx2 = await tokenContract.approve(tokenExchangeAddress, ethers.constants.MaxUint256);
        await tx2.wait()
        const tx3 = await tokenExchange.addLiquidity(tokenDepositAmountBN, tokenDepositAmountBN, 1739591241, { ...TX_OVERRIDES, value: tokenValueInEtherBN });
        await tx3.wait()
        return tokenExchange
    }

    private async tokenExchange(name: string, symbol: string, amount: number, tokenDepositAmount?: number, tokenValueInEther?: number): Promise<TokenExchange> {
        const token = await this.deployToken(name, symbol, amount)
        const exchange = await this.createExchange(token.address, tokenDepositAmount, tokenValueInEther)
        return {
            tokenAddress: token.address,
            exchangeAddress: exchange.address
        }
    }


    private async deployRabalance(tokenExhanges: TokenExchange[]): Promise<Contract> {
        let tokenAddressesArray = []
        let exchangeAddressesArray = []
        for (const i in tokenExhanges) {
            tokenAddressesArray.push(tokenExhanges[i].tokenAddress)
            exchangeAddressesArray.push(tokenExhanges[i].exchangeAddress)
        }
        const rebalanceContractFactory = new ethers.ContractFactory(REBALANCE_JSON.abi, REBALANCE_JSON.bytecode, this.signer);
        this.REBALANCE_CONTRACT = await rebalanceContractFactory.deploy(tokenAddressesArray, exchangeAddressesArray, exchangeAddressesArray.length, TX_OVERRIDES);
        return await this.REBALANCE_CONTRACT.deployed();
    }

}

