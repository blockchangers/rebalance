pragma solidity ^0.5.0;

import "./../node_modules/@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "./../node_modules/@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";

contract Token is ERC20, ERC20Detailed {
    constructor (string memory name, string memory symbol, uint256 amount) public ERC20Detailed(name, symbol, 18) {
        _mint(msg.sender, amount * (10 ** uint256(decimals())));
    }
}
