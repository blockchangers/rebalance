import { ethers } from 'ethers';

import { actions } from './rebalance/actions.rebalance'
import { getters } from './rebalance/getters.rebalance'
import { mutations } from './rebalance/mutations.rebalance'

export default {
    state: {
        tryAgain: true,
        messages: [],
        tokenList: [],
        tokens: {},
        tokenExchangesSupported: {
            1: [ // Mainet
                {
                    "tokenAddress": "0x960b236A07cf122663c4303350609A66A7B288C0",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x960b236a07cf122663c4303350609a66a7b288c0.png",
                    "ticker": "ANT",
                    "exchangeAddress": "0x077d52b047735976dfda76fef74d4d988ac25196"
                },
                {
                    "tokenAddress": "0x0D8775F648430679A709E98d2b0Cb6250d2887EF",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x0d8775f648430679a709e98d2b0cb6250d2887ef.png",
                    "ticker": "BAT",
                    "exchangeAddress": "0x2e642b8d59b45a1d8c5aef716a84ff44ea665914"
                },
                {
                    "tokenAddress": "0x107c4504cd79C5d2696Ea0030a8dD4e92601B82e",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x107c4504cd79c5d2696ea0030a8dd4e92601b82e.png",
                    "ticker": "BLT",
                    "exchangeAddress": "0x0e6a53b13688018a3df8c69f99afb19a3068d04f"
                },
                {
                    "tokenAddress": "0x1F573D6Fb3F13d689FF844B4cE37794d79a7FF1C",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c.png",
                    "ticker": "BNT",
                    "exchangeAddress": "0x87d80dbd37e551f58680b4217b23af6a752da83f"
                },
                {
                    "tokenAddress": "0x26E75307Fc0C021472fEb8F727839531F112f317",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x26e75307fc0c021472feb8f727839531f112f317.png",
                    "ticker": "C20",
                    "exchangeAddress": "0xf7b5a4b934658025390ff69db302bc7f2ac4a542"
                },
                {
                    "tokenAddress": "0xF5DCe57282A584D2746FaF1593d3121Fcac444dC",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xf5dce57282a584d2746faf1593d3121fcac444dc.png",
                    "ticker": "cDAI",
                    "exchangeAddress": "0x45a2fdfed7f7a2c791fb1bdf6075b83fad821dde"
                },
                {
                    "tokenAddress": "0x41e5560054824eA6B0732E656E3Ad64E20e94E45",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x41e5560054824ea6b0732e656e3ad64e20e94e45.png",
                    "ticker": "CVC",
                    "exchangeAddress": "0x1c6c712b1f4a7c263b1dbd8f97fb447c945d3b9a"
                },
                {
                    "tokenAddress": "0x89d24A6b4CcB1B6fAA2625fE562bDD9a23260359",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359.png",
                    "ticker": "DAI",
                    "exchangeAddress": "0x09cabec1ead1c0ba254b09efb3ee13841712be14"
                },
                {
                    "tokenAddress": "0xE0B7927c4aF23765Cb51314A0E0521A9645F0E2A",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xe0b7927c4af23765cb51314a0e0521a9645f0e2a.png",
                    "ticker": "DGD",
                    "exchangeAddress": "0xd55c1ca9f5992a2e5e379dce49abf24294abe055"
                },
                {
                    "tokenAddress": "0x4f3AfEC4E5a3F2A6a1A411DEF7D7dFe50eE057bF",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x4f3afec4e5a3f2a6a1a411def7d7dfe50ee057bf.png",
                    "ticker": "DGX",
                    "exchangeAddress": "0xb92de8b30584392af27726d5ce04ef3c4e5c9924"
                },
                {
                    "tokenAddress": "0x4946Fcea7C692606e8908002e55A582af44AC121",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x4946fcea7c692606e8908002e55a582af44ac121.png",
                    "ticker": "FOAM",
                    "exchangeAddress": "0xf79cb3bea83bd502737586a6e8b133c378fd1ff2"
                },
                {
                    "tokenAddress": "0x419D0d8BdD9aF5e606Ae2232ed285Aff190E711b",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x419d0d8bdd9af5e606ae2232ed285aff190e711b.png",
                    "ticker": "FUN",
                    "exchangeAddress": "0x60a87cc7fca7e53867facb79da73181b1bb4238b"
                },
                {
                    "tokenAddress": "0x543Ff227F64Aa17eA132Bf9886cAb5DB55DCAddf",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x543ff227f64aa17ea132bf9886cab5db55dcaddf.png",
                    "ticker": "GEN",
                    "exchangeAddress": "0x26cc0eab6cb650b0db4d0d0da8cb5bf69f4ad692"
                },
                {
                    "tokenAddress": "0x6810e776880C02933D47DB1b9fc05908e5386b96",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x6810e776880c02933d47db1b9fc05908e5386b96.png",
                    "ticker": "GNO",
                    "exchangeAddress": "0xe8e45431b93215566ba923a7e611b7342ea954df"
                },
                {
                    "tokenAddress": "0x12B19D3e2ccc14Da04FAe33e63652ce469b3F2FD",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x12b19d3e2ccc14da04fae33e63652ce469b3f2fd.png",
                    "ticker": "GRID",
                    "exchangeAddress": "0x4b17685b330307c751b47f33890c8398df4fe407"
                },
                {
                    "tokenAddress": "0x6c6EE5e31d828De241282B9606C8e98Ea48526E2",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x6c6ee5e31d828de241282b9606c8e98ea48526e2.png",
                    "ticker": "HOT",
                    "exchangeAddress": "0xd4777e164c6c683e10593e08760b803d58529a8e"
                },
                {
                    "tokenAddress": "0x818Fc6C2Ec5986bc6E2CBf00939d90556aB12ce5",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x818fc6c2ec5986bc6e2cbf00939d90556ab12ce5.png",
                    "ticker": "KIN",
                    "exchangeAddress": "0xb7520a5f8c832c573d6bd0df955fc5c9b72400f7"
                },
                {
                    "tokenAddress": "0xdd974D5C2e2928deA5F71b9825b8b646686BD200",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xdd974d5c2e2928dea5f71b9825b8b646686bd200.png",
                    "ticker": "KNC",
                    "exchangeAddress": "0x49c4f9bc14884f6210f28342ced592a633801a8b"
                },
                {
                    "tokenAddress": "0x514910771AF9Ca656af840dff83E8264EcF986CA",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x514910771af9ca656af840dff83e8264ecf986ca.png",
                    "ticker": "LINK",
                    "exchangeAddress": "0xf173214c720f58e03e194085b1db28b50acdeead"
                },
                {
                    "tokenAddress": "0xA4e8C3Ec456107eA67d3075bF9e3DF3A75823DB0",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xa4e8c3ec456107ea67d3075bf9e3df3a75823db0.png",
                    "ticker": "LOOM",
                    "exchangeAddress": "0x417cb32bc991fbbdcae230c7c4771cc0d69daa6b"
                },
                {
                    "tokenAddress": "0x58b6A8A3302369DAEc383334672404Ee733aB239",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x58b6a8a3302369daec383334672404ee733ab239.png",
                    "ticker": "LPT",
                    "exchangeAddress": "0xc4a1c45d5546029fd57128483ae65b56124bfa6a"
                },
                {
                    "tokenAddress": "0xD29F0b5b3F50b07Fe9a9511F7d86F4f4bAc3f8c4",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xd29f0b5b3f50b07fe9a9511f7d86f4f4bac3f8c4.png",
                    "ticker": "LQD",
                    "exchangeAddress": "0xe3406e7d0155e0a83236ec25d34cd3d903036669"
                },
                {
                    "tokenAddress": "0xBBbbCA6A901c926F240b89EacB641d8Aec7AEafD",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xbbbbca6a901c926f240b89eacb641d8aec7aeafd.png",
                    "ticker": "LRC",
                    "exchangeAddress": "0xa539baaa3aca455c986bb1e25301cef936ce1b65"
                },
                {
                    "tokenAddress": "0x0F5D2fB29fb7d3CFeE444a200298f468908cC942",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x0f5d2fb29fb7d3cfee444a200298f468908cc942.png",
                    "ticker": "MANA",
                    "exchangeAddress": "0xc6581ce3a005e2801c1e0903281bbd318ec5b5c2"
                },
                {
                    "tokenAddress": "0x7D1AfA7B718fb893dB30A3aBc0Cfc608AaCfeBB0",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x7d1afa7b718fb893db30a3abc0cfc608aacfebb0.png",
                    "ticker": "MATIC",
                    "exchangeAddress": "0x9a7a75e66b325a3bd46973b2b57c9b8d9d26a621"
                },
                {
                    "tokenAddress": "0x80f222a749a2e18Eb7f676D371F19ad7EFEEe3b7",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x80f222a749a2e18eb7f676d371f19ad7efeee3b7.png",
                    "ticker": "MGN",
                    "exchangeAddress": "0xdd80ca8062c7ef90fca2547e6a2a126c596e611f"
                },
                {
                    "tokenAddress": "0x9f8F72aA9304c8B593d555F12eF6589cC3A579A2",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2.png",
                    "ticker": "MKR",
                    "exchangeAddress": "0x2c4bd064b998838076fa341a83d007fc2fa50957"
                },
                {
                    "tokenAddress": "0xec67005c4E498Ec7f55E092bd1d35cbC47C91892",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xec67005c4e498ec7f55e092bd1d35cbc47c91892.png",
                    "ticker": "MLN",
                    "exchangeAddress": "0xa931f4eb165ac307fd7431b5ec6eadde53e14b0c"
                },
                {
                    "tokenAddress": "0x957c30aB0426e0C93CD8241E2c60392d08c6aC8e",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x957c30ab0426e0c93cd8241e2c60392d08c6ac8e.png",
                    "ticker": "MOD",
                    "exchangeAddress": "0xccb98654cd486216fff273dd025246588e77cfc1"
                },
                {
                    "tokenAddress": "0xB62132e35a6c13ee1EE0f84dC5d40bad8d815206",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xb62132e35a6c13ee1ee0f84dc5d40bad8d815206.png",
                    "ticker": "NEXO",
                    "exchangeAddress": "0x069c97dba948175d10af4b2414969e0b88d44669"
                },
                {
                    "tokenAddress": "0x1776e1F26f98b1A5dF9cD347953a26dd3Cb46671",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x1776e1f26f98b1a5df9cd347953a26dd3cb46671.png",
                    "ticker": "NMR",
                    "exchangeAddress": "0x2bf5a5ba29e60682fc56b2fcf9ce07bef4f6196f"
                },
                {
                    "tokenAddress": "0x8E870D67F660D95d5be530380D0eC0bd388289E1",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x8e870d67f660d95d5be530380d0ec0bd388289e1.png",
                    "ticker": "PAX",
                    "exchangeAddress": "0xc040d51b07aea5d94a89bc21e8078b77366fc6c7"
                },
                {
                    "tokenAddress": "0x93ED3FBe21207Ec2E8f2d3c3de6e058Cb73Bc04d",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x93ed3fbe21207ec2e8f2d3c3de6e058cb73bc04d.png",
                    "ticker": "PNK",
                    "exchangeAddress": "0xf506828b166de88ca2edb2a98d960abba0d2402a"
                },
                {
                    "tokenAddress": "0x6758B7d441a9739b98552B373703d8d3d14f9e62",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x6758b7d441a9739b98552b373703d8d3d14f9e62.png",
                    "ticker": "POA20",
                    "exchangeAddress": "0xa2e6b3ef205feaee475937c4883b24e6eb717eef"
                },
                {
                    "tokenAddress": "0x687BfC3E73f6af55F0CccA8450114D107E781a0e",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x687bfc3e73f6af55f0ccca8450114d107e781a0e.png",
                    "ticker": "QCH",
                    "exchangeAddress": "0x755899f0540c3548b99e68c59adb0f15d2695188"
                },
                {
                    "tokenAddress": "0x255Aa6DF07540Cb5d3d297f0D0D4D84cb52bc8e6",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x255aa6df07540cb5d3d297f0d0d4d84cb52bc8e6.png",
                    "ticker": "RDN",
                    "exchangeAddress": "0x7d03cecb36820b4666f45e1b4ca2538724db271c"
                },
                {
                    "tokenAddress": "0x408e41876cCCDC0F92210600ef50372656052a38",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x408e41876cccdc0f92210600ef50372656052a38.png",
                    "ticker": "REN",
                    "exchangeAddress": "0x43892992b0b102459e895b88601bb2c76736942c"
                },
                {
                    "tokenAddress": "0x1985365e9f78359a9B6AD760e32412f4a445E862",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x1985365e9f78359a9b6ad760e32412f4a445e862.png",
                    "ticker": "REP",
                    "exchangeAddress": "0x43892992b0b102459e895b88601bb2c76736942c"
                },
                {
                    "tokenAddress": "0x168296bb09e24A88805CB9c33356536B980D3fC5",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x168296bb09e24a88805cb9c33356536b980d3fc5.png",
                    "ticker": "RHOC",
                    "exchangeAddress": "0x394e524b47a3ab3d3327f7ff6629dc378c1494a3"
                },
                {
                    "tokenAddress": "0x607F4C5BB672230e8672085532f7e901544a7375",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x607f4c5bb672230e8672085532f7e901544a7375.png",
                    "ticker": "RLC",
                    "exchangeAddress": "0xa825cae02b310e9901b4776806ce25db520c8642"
                },
                {
                    "tokenAddress": "0xB4EFd85c19999D84251304bDA99E90B92300Bd93",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xb4efd85c19999d84251304bda99e90b92300bd93.png",
                    "ticker": "RPL",
                    "exchangeAddress": "0x3fb2f18065926ddb33e7571475c509541d15da0e"
                },
                {
                    "tokenAddress": "0x4156D3342D5c385a87D264F90653733592000581",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x4156d3342d5c385a87d264f90653733592000581.png",
                    "ticker": "SALT",
                    "exchangeAddress": "0x4156D3342D5c385a87D264F90653733592000581"
                },
                {
                    "tokenAddress": "0x42456D7084eacF4083f1140d3229471bbA2949A8",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x42456d7084eacf4083f1140d3229471bba2949a8.png",
                    "ticker": "sETH",
                    "exchangeAddress": "0x4740c758859d4651061cc9cdefdba92bdc3a845d"
                },
                {
                    "tokenAddress": "0x744d70FDBE2Ba4CF95131626614a1763DF805B9E",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x744d70fdbe2ba4cf95131626614a1763df805b9e.png",
                    "ticker": "SNT",
                    "exchangeAddress": "0x1aec8f11a7e78dc22477e91ed924fab46e3a88fd"
                },
                {
                    "tokenAddress": "0x42d6622deCe394b54999Fbd73D108123806f6a18",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x42d6622dece394b54999fbd73d108123806f6a18.png",
                    "ticker": "SPANK",
                    "exchangeAddress": "0x4e395304655f0796bc3bc63709db72173b9ddf98"
                },
                {
                    "tokenAddress": "0xB64ef51C888972c908CFacf59B47C1AfBC0Ab8aC",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xb64ef51c888972c908cfacf59b47c1afbc0ab8ac.png",
                    "ticker": "STORJ",
                    "exchangeAddress": "0xa7298541e52f96d42382ecbe4f242cbcbc534d02"
                },
                {
                    "tokenAddress": "0x0cbe2df57ca9191b64a7af3baa3f946fa7df2f25",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x0cbe2df57ca9191b64a7af3baa3f946fa7df2f25.png",
                    "ticker": "sUSD",
                    "exchangeAddress": "0xa1ecdcca26150cf69090280ee2ee32347c238c7b"
                },
                {
                    "tokenAddress": "0xaAAf91D9b90dF800Df4F55c205fd6989c977E73a",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xaaaf91d9b90df800df4f55c205fd6989c977e73a.png",
                    "ticker": "TKN",
                    "exchangeAddress": "0xb6cfbf322db47d39331e306005dc7e5e6549942b"
                },
                {
                    "tokenAddress": "0x8dd5fbCe2F6a956C3022bA3663759011Dd51e73E",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x8dd5fbce2f6a956c3022ba3663759011dd51e73e.png",
                    "ticker": "TUSD",
                    "exchangeAddress": "0x4f30e682d0541eac91748bd38a648d759261b8f3"
                },
                {
                    "tokenAddress": "0x09cabEC1eAd1c0Ba254B09efb3EE13841712bE14",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x09cabec1ead1c0ba254b09efb3ee13841712be14.png",
                    "ticker": "UNI-V1:DAI",
                    "exchangeAddress": "0x601c32e0580d3aef9437db52d09f5a5d7e60ec22"
                },
                {
                    "tokenAddress": "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48.png",
                    "ticker": "USDC",
                    "exchangeAddress": "0x97dec872013f6b5fb443861090ad931542878126"
                },
                {
                    "tokenAddress": "0x8f3470A7388c05eE4e7AF3d01D8C722b0FF52374",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x8f3470a7388c05ee4e7af3d01d8c722b0ff52374.png",
                    "ticker": "VERI",
                    "exchangeAddress": "0x17e5bf07d696eaf0d14caa4b44ff8a1e17b34de3"
                },
                {
                    "tokenAddress": "0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x2260fac5e5542a773aa44fbcfedf7c193bc2c599.png",
                    "ticker": "WBTC",
                    "exchangeAddress": "0x4d2f5cfba55ae412221182d8475bc85799a5644b"
                },
                {
                    "tokenAddress": "0x09fE5f0236F0Ea5D930197DCE254d77B04128075",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x09fe5f0236f0ea5d930197dce254d77b04128075.png",
                    "ticker": "WCK",
                    "exchangeAddress": "0x4ff7fa493559c40abd6d157a0bfc35df68d8d0ac"
                },
                {
                    "tokenAddress": "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2.png",
                    "ticker": "WETH",
                    "exchangeAddress": "0xa2881a90bf33f03e7a3f803765cd2ed5c8928dfb"
                },
                {
                    "tokenAddress": "0xB4272071eCAdd69d933AdcD19cA99fe80664fc08",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xb4272071ecadd69d933adcd19ca99fe80664fc08.png",
                    "ticker": "XCHF",
                    "exchangeAddress": "0x8de0d002dc83478f479dc31f76cb0a8aa7ccea17"
                },
                {
                    "tokenAddress": "0xE41d2489571d322189246DaFA5ebDe1F4699F498",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xe41d2489571d322189246dafa5ebde1f4699f498.png",
                    "ticker": "ZRX",
                    "exchangeAddress": "0xae76c84c9262cdb9abc0c2c8888e62db8e22a0bf"
                }
            ],
            4: [ // Rinkeby
                {
                    "tokenAddress": "0xDA5B056Cfb861282B4b59d29c9B395bcC238D29B",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x0d8775f648430679a709e98d2b0cb6250d2887ef.png",
                    "ticker": "BAT",
                    "exchangeAddress": "0x9B913956036a3462330B0642B20D3879ce68b450"
                },
                {
                    "tokenAddress": "0x2448eE2641d78CC42D7AD76498917359D961A783",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359.png",
                    "ticker": "DAI",
                    "exchangeAddress": "0x77dB9C915809e7BE439D2AB21032B1b8B58F6891"
                },
                {
                    "tokenAddress": "0xF9bA5210F91D0474bd1e1DcDAeC4C58E359AaD85",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2.png",
                    "ticker": "MKR",
                    "exchangeAddress": "0x93bB63aFe1E0180d0eF100D774B473034fd60C36"
                },
                {
                    "tokenAddress": "0xF22e3F33768354c9805d046af3C0926f27741B43",
                    "image": "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0xe41d2489571d322189246dafa5ebde1f4699f498.png",
                    "ticker": "ZRX",
                    "exchangeAddress": "0xaBD44a1D1b9Fb0F39fE1D1ee6b1e2a14916D067D"
                }
            ]
        },
        totalValueETH: ethers.constants.Zero._hex,
        groupList: [],
        groups: {},
        ready: false,
        rebalanceContractAddress: {
            1: "0x1c3031e02e9a0ec237f5bd674eaaa5e56a2ae174",
            4: "0xb90be6f36d1A4517aBda0160777d64B467b39A34"
        },
        topTokensInfo: [],
        ethPriceUSD: 0
    },
    getters,
    actions,
    mutations,
    namespaced: true
};