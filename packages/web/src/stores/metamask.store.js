import Vue from "vue";
// Get Browser ethereum client
let ethereum
if (typeof window.ethereum !== 'undefined' || (typeof window.web3 !== 'undefined')) {
    ethereum = window.ethereum
}


const production = () => {
    return process.env.NODE_ENV === 'production'
}

export default {
    state: {
        network: null,
        accounts: [],
        error: null,
        foundEthereum: false,
        listeners: false,
    },
    getters: {
        currentAccount: state => state.accounts[0],
        notDesiredNetwork: state => (state.network !== state.networkDesired),
        ready: state => {
            return (state.network !== null) &&
                state.accounts > 0
        }
    },
    actions: {
        // Called to initiate the model
        async init({ commit, state }) {
            return new Promise(async (resolve, reject) => {
                // Stop if Metamask not found
                commit('foundEthereum', { boolean: (ethereum !== undefined) })
                if (!state.foundEthereum) resolve(false)

                // Since we have metamask, we can now add listeners
                const changeAccount = (accounts) => {
                    commit('accounts', { accounts })
                }
                //ethereum.removeEventListener('accountsChanged', changeAccount)
                ethereum.on('accountsChanged', changeAccount)
                const changeNetwork = (network) => {
                    commit('network', { network: network })
                }
                // ethereum.removeEventListener('networkChanged', changeNetwork)
                ethereum.on('networkChanged', changeNetwork)

                // Stop if not correct network
                commit('network', { network: ethereum.networkVersion })

                setTimeout(() => {
                    reject("No respons from Metamask")
                }, 50000)
                const accounts = await ethereum.enable()
                commit('accounts', { accounts })
                resolve(true)
            })
        },
    },
    mutations: {
        accounts(state, { accounts }) {
            Vue.set(state, 'accounts', [...accounts]);
        },
        network(state, { network }) {
            state.network = network
        },
        foundEthereum(state, { boolean }) {
            state.foundEthereum = boolean
        },
        initialized(state, { initialized }) {
            state.initialized = initialized
        },
        listeners(state, { listeners }) {
            state.listeners = listeners
        },
    },
    namespaced: true
};
