import { get } from "axios";
import { ethers } from 'ethers';
import { bigNumberify } from 'ethers/utils';
import { Deployer, CONSTANTS, TOKEN_JSON, REBALANCE_JSON } from "@rebalance/contracts";

// Get Browser ethereum client
let ethereum
if (typeof window.ethereum !== 'undefined' || (typeof window.web3 !== 'undefined')) {
    ethereum = window.ethereum
}
const ETHPLORER_API_KEY = 'freekey'
let DEFAULT_GROUP_LIST = []
if (process.env.NODE_ENV === 'production') {
    DEFAULT_GROUP_LIST = ['default'/* , 'low', 'medium', 'high' */]
} else {
    DEFAULT_GROUP_LIST = [/* 'low', 'medium',  */'default'/* , 'high' */]
}
const DEFAULT_GROUPS = {
    'default': {
        id: 'default',
        name: "Default group",
        tokens: [],
        currentBalanceEth: ethers.constants.Zero._hex,
        desiredBalanceEth: ethers.constants.Zero._hex,
        currentBalancePercentage: 0,
        desiredBalancePercentage: 0,
        currentBalanceUSD: 0,
        desiredBalanceUSD: 0,
    },
    'low': {
        id: 'low',
        name: "Low risk",
        tokens: [],
        currentBalanceEth: ethers.constants.Zero._hex,
        desiredBalanceEth: ethers.constants.Zero._hex,
        currentBalancePercentage: 0,
        desiredBalancePercentage: 0,
        currentBalanceUSD: 0,
        desiredBalanceUSD: 0,
    },
    'medium': {
        id: 'medium',
        name: "Medium risk",
        tokens: [],
        currentBalanceEth: ethers.constants.Zero._hex,
        desiredBalanceEth: ethers.constants.Zero._hex,
        currentBalancePercentage: 0,
        desiredBalancePercentage: 0,
        currentBalanceUSD: 0,
        desiredBalanceUSD: 0,
    },
    'high': {
        id: 'high',
        name: "High risk",
        tokens: [],
        currentBalanceEth: ethers.constants.Zero._hex,
        desiredBalanceEth: ethers.constants.Zero._hex,
        currentBalancePercentage: 0,
        desiredBalancePercentage: 0,
        currentBalanceUSD: 0,
        desiredBalanceUSD: 0,
    }
}

export const actions = {
    //  Procedures
    init({ dispatch, state, commit }) {
        return new Promise(async (resolve, reject) => {
            await ethereum.enable()
            try {
                await dispatch('initExchangesSupported')
                console.log("LOG: initExchangesSupported")
                await dispatch('initTokenBalances')
                console.log("LOG: initTokenBalances")
                await dispatch('initEthBalance')
                console.log("LOG: initEthBalance")
                await dispatch('initTotalBalances')
                console.log("LOG: initTotalBalances")
                await dispatch('initGroups')
                console.log("LOG: initGroups")
                dispatch('initEthPriceUSD')
                console.log("LOG: initEthPriceUSD")
                await dispatch('initPercentages')
                console.log("LOG: initPercentages")
                commit('ready', true)
                resolve()

            } catch (error) {
                console.log("REBALANCE STORE ERROR: ", error)
                // setTimeout(() => {
                //     if (state.tryAgain) {
                //         dispatch('init')
                //     }
                // }, 2000)
                reject(error)
                return
            }
        })
    },
    initExchangesSupported({ state, rootState, commit }) {
        return new Promise(async (resolve, reject) => {
            const network = rootState.metamask.network
            console.log("On Network", network)
            // stop init if not network found
            if (!parseInt(network) > 0) {
                // Stop and return signal that rebalance was not initilized
                commit('tryAgain', true)
                reject("No metamask account found")
                return;
            }
            if (!state.tokenExchangesSupported.hasOwnProperty(network)) {
                // Its not preconfigured, lets try to get it from localstorage
                const tokenExchangesSupportedFromLocalStorage = JSON.parse(localStorage.getItem('tokenExchangesSupported' + network))
                const rebalanceContractAddressFromLocalStorage = JSON.parse(localStorage.getItem('rebalanceContractAddress' + network))

                /* 
                
                TODO : MUST ADD a check for rebalanceContractAddressFromLocalStorage also. This should be handled outside next if function. Must change inside the IF function. 
                
                */

                if (tokenExchangesSupportedFromLocalStorage && tokenExchangesSupportedFromLocalStorage.hasOwnProperty(network)) {
                    commit('addExchangesToNetwork', {
                        network,
                        exchanges: tokenExchangesSupportedFromLocalStorage[network]
                    })
                    commit('addRebalanceContractAddress', {
                        network,
                        address: rebalanceContractAddressFromLocalStorage[network]
                    })
                } else {
                    // If its not in localstorage either, lets create the full setup
                    const confirmation = confirm("Do you want to create full rebalance setup?")
                    if (!confirmation) {
                        // Stop and return signal that rebalance was initilized
                        commit('tryAgain', false)
                        reject("Could not find any tokenExchangesSupported on your connected Network. Nothing deployed as per requested. Refresh site to start over.")
                        return;
                    }
                    const deployer = new Deployer(ethereum)
                    const rebalancePackage = await deployer.setup()
                    console.log("Done setup with ", rebalancePackage.tokenExchanges.length, " exchanges. Its now stored in localstorage so you dont need to redeploy all the time.")
                    commit('addExchangesToNetwork', {
                        network,
                        exchanges: rebalancePackage.tokenExchanges
                    })
                    commit('addRebalanceContractAddress', {
                        network,
                        address: rebalancePackage.rebalanceAddress
                    })
                    const ExchangesStorageArray = (tokenExchangesSupportedFromLocalStorage) ? { ...tokenExchangesSupportedFromLocalStorage, [network]: rebalancePackage.tokenExchanges } : { [network]: rebalancePackage.tokenExchanges }
                    localStorage.setItem('tokenExchangesSupported' + network, JSON.stringify(ExchangesStorageArray))

                    const rebalanceContractAddressStorageArray = (tokenExchangesSupportedFromLocalStorage) ? { ...rebalanceContractAddressFromLocalStorage, [network]: rebalancePackage.rebalanceAddress } : { [network]: rebalancePackage.rebalanceAddress }
                    localStorage.setItem('rebalanceContractAddress' + network, JSON.stringify(rebalanceContractAddressStorageArray))
                }
                commit('tryAgain', false)
                console.log("TokenExchangesSupported has been found.")
                resolve()
            } else {
                // Mainnet and Rinkeby and other hardcoded. Proabably dont have to do anything
                resolve()
            }
        })
    },
    initTokenBalances({ commit, rootState, dispatch, state }) {
        return new Promise(async (resolve, reject) => {
            const accounts = rootState.metamask.accounts
            // stop init if not network found
            if (!Array.isArray(accounts) || !accounts.length) {
                // Stop and return signal that rebalance was not initilized
                commit('tryAgain', true)
                reject("No metamask account found")
            }

            // lets just get some random data from Vitaliks Eth address
            const accountInfo = await dispatch('getAddressInfo', (process.env.NODE_ENV === 'production') ? accounts[0] : '0xab5801a7d398351b8be11c439e05c5b3259aec9b')
            // Testnet we need to gather each balance
            const web3Provider = new ethers.providers.Web3Provider(ethereum)
            if (!Array.isArray(state.tokenExchangesSupported[rootState.metamask.network])) {
                commit('tryAgain', true)
                reject("No tokenExchangesSupported for network found when getting balances.")
            }
            await Promise.all(state.tokenExchangesSupported[rootState.metamask.network].map(tokenExchangeData => {
                return new Promise(async (resolve) => {

                    const tokenExchange = new ethers.Contract(tokenExchangeData.exchangeAddress, CONSTANTS.exchangeByteAbi, web3Provider.getSigner());
                    // TODO: FIX ETHPLORER
                    const ethplorerToken = null //(accountInfo.hasOwnProperty('tokens')) ? accountInfo.tokens.find(tokenInfo => tokenInfo.address === tokenExchangeData.tokenAddress) : false

                    let balance = ethers.constants.Zero
                    let symbol = "XXX"
                    let priceUSD = ethers.constants.Zero
                    let valueInEth = ethers.constants.Zero
                    let image = null
                    if (ethplorerToken) {
                        // Sweet, we can use information from ethplorer if this user has this token
                        const token = accountInfo.tokens.find(tokenInfo => tokenInfo.address === tokenExchangeData.tokenAddress)
                        balance = token.balance
                        symbol = token.symbol
                        priceUSD = token.price.rate
                    } else {
                        // Get this information from contract
                        const tokenContract = new ethers.Contract(tokenExchangeData.tokenAddress, TOKEN_JSON.abi, web3Provider.getSigner())
                        balance = await tokenContract.balanceOf(accounts[0])
                        if (tokenExchangeData.hasOwnProperty('ticker')) {
                            symbol = tokenExchangeData.ticker
                        } else {
                            try {
                                symbol = await tokenContract.symbol()
                            } catch (error) {
                                console.log("Could not set symbol")
                            }
                        }

                        // Get image
                        if (tokenExchangeData.hasOwnProperty('image')) {
                            image = tokenExchangeData.image
                        } else {
                            // Just return an image sometimes in test
                            image = (Math.random() >= 0.5) ? null : "https://raw.githubusercontent.com/TrustWallet/tokens/master/tokens/0x960b236a07cf122663c4303350609a66a7b288c0.png"
                        }



                        // TODO: FIX ETHPLORER
                        const tokenInfo = null // await dispatch('getTokenInfo', (process.env.NODE_ENV === 'production') ? tokenExchangeData.tokenAddress : '0x0d8775f648430679a709e98d2b0cb6250d2887ef')
                        //  const tokenInfo = await dispatch('getTokenInfo', tokenExchangeData.tokenAddress )
                        if (tokenInfo && !tokenInfo.data.hasOwnProperty('error')) {
                            priceUSD = tokenInfo.data.price.rate
                        }
                    }
                    if (!balance.eq(ethers.constants.Zero)) {
                        try {
                            valueInEth = await tokenExchange.getTokenToEthInputPrice(balance)
                        } catch (error) {
                            console.log("Skipping", symbol, "with balance", balance.toString())
                        }
                    }
                    const priceUSD_BN = dispatch('decimalToBn', priceUSD)
                    commit('addToken', {
                        type: "token",
                        tokenAddress: tokenExchangeData.tokenAddress,
                        exchangeAddress: tokenExchangeData.exchangeAddress,
                        priceUSD: priceUSD_BN._hex,
                        currentBalanceToken: balance._hex,
                        desiredBalanceToken: balance._hex,
                        currentBalanceEth: valueInEth._hex,
                        desiredBalanceEth: valueInEth._hex,
                        currentBalanceUSD: 0,
                        desiredBalanceUSD: 0,
                        allowance: ethers.constants.Zero,
                        symbol: symbol,
                        currentBalancePercentage: 0,
                        desiredBalancePercentage: 0,
                        color: await dispatch('getRandomColor'),
                        image: image
                    })
                    resolve()
                })
            }))
            resolve()
        })
    },
    initEthBalance({ rootState, commit, dispatch }) {
        return new Promise(async (resolve, reject) => {
            // We need to add eth
            const accounts = rootState.metamask.accounts
            // stop init if not network found
            if (!Array.isArray(accounts) || !accounts.length) {
                // Stop and return signal that rebalance was not initilized
                commit('tryAgain', true)
                reject("No metamask account found")
            }
            const web3Provider = new ethers.providers.Web3Provider(ethereum)
            const ethBalance = await web3Provider.getBalance(accounts[0])
            const tokens = await dispatch('getTopTokenInfo', 100)
            const ethInfo = tokens.find(tokenInfo => tokenInfo.name === "Ethereum" && tokenInfo.address === "0x0000000000000000000000000000000000000000")
            if (!ethInfo) {
                commit('tryAgain', true)
                reject("Could not get Eth information from Ethplorer. Will try again.")
            }
            const priceUSD_BN = await dispatch('decimalToBn', ethInfo.price.rate)
            commit('addToken', {
                type: "ETH",
                tokenAddress: "0x0000000000000000000000000000000000000000",
                exchangeAddress: "NOT RELEVANT",
                priceUSD: priceUSD_BN,
                currentBalanceToken: ethBalance._hex,
                desiredBalanceToken: ethBalance._hex,
                currentBalanceEth: ethBalance._hex,
                desiredBalanceEth: ethBalance._hex,
                currentBalancePercentage: 0,
                desiredBalancePercentage: 0,
                currentBalanceUSD: 0,
                desiredBalanceUSD: 0,
                allowance: ethers.constants.MaxUint256,
                symbol: "ETH",
                color: '#00dba9',
                image: "https://etherscan.io/images/ethereum-icon.png"
            })
            //End 
            resolve()
        })
    },
    async initTotalBalances({ state, commit }) {
        return new Promise(async (resolve) => {
            const totalValueETHBN = Object.keys(state.tokens).reduce((prev, index) => {
                return prev.then(BN => {
                    const valueBN = bigNumberify(state.tokens[index].desiredBalanceEth)
                    console.log("checking ", state.tokens[index].symbol, "to calc total. Value is ", ethers.utils.formatEther(valueBN))
                    return new Promise(resolve => resolve(BN.add(valueBN)))
                })
            }, Promise.resolve(ethers.constants.Zero))
            const totalValueETH = await totalValueETHBN
            commit('totalValueETH', totalValueETH)
            resolve()
        })
    },
    async initGroups({ commit, state, dispatch }) {
        return await Promise.all(DEFAULT_GROUP_LIST.map(groupId => {
            return new Promise(async resolve => {

                let tokens = []
                // If development and low group, add every token, but only two
                // if (process.env.NODE_ENV !== 'production' && groupId === "low") {
                //     tokens = state.tokenList.slice(0, 1)
                // }
                // if (process.env.NODE_ENV !== 'production' && groupId === "medium") {
                //     tokens = state.tokenList.slice(1, 3)
                // }
                // if (process.env.NODE_ENV !== 'production' && groupId === "medium") {
                //     tokens = state.tokenList.slice(0, 4)
                // }
                if (groupId === "default") {
                    for (let tokenIndex in state.tokenList) {
                        const token = state.tokens[state.tokenList[tokenIndex]]
                        if (bigNumberify(token.desiredBalanceEth).gt(ethers.constants.Zero)) {
                            tokens.push(token.tokenAddress)
                        }
                    }
                }
                const valueBN = await dispatch('fromTokenAddressesGetDesiredBalanceEth', tokens)

                commit('addGroup', { ...DEFAULT_GROUPS[groupId], ...{ tokens: tokens, currentBalanceEth: valueBN._hex, desiredBalanceEth: valueBN._hex } })

                resolve()
            })
        }))
    },
    async initPercentages({ commit, dispatch, state }) {
        const allGroupsDesiredBalanceEthBN = await dispatch('getAllGroupsDesiredBalanceEth')
        if (allGroupsDesiredBalanceEthBN.eq(ethers.constants.Zero)) {
            return Promise.resolve()
        }
        return await Promise.all(state.groupList.map(groupId => {
            return new Promise(async resolve => {
                const groupDesiredBalanceEthBN = bigNumberify(state.groups[groupId].desiredBalanceEth)
                const groupPercentageFLOAT = (await dispatch('bnToDecimal', groupDesiredBalanceEthBN) / await dispatch('bnToDecimal', allGroupsDesiredBalanceEthBN)) * 100
                const groupDesiredBalanceUSD = await dispatch('BnEthToUSD', { bn: groupDesiredBalanceEthBN })
                commit('setGroupDesiredBalancePercentage', {
                    groupId: groupId,
                    number: groupPercentageFLOAT
                })
                commit('setGroupCurrentBalancePercentage', {
                    groupId: groupId,
                    number: groupPercentageFLOAT
                })
                commit("setGroupDesiredBalanceUSD", {
                    groupId: groupId,
                    number: groupDesiredBalanceUSD,
                })
                resolve(await Promise.all(state.groups[groupId].tokens.map(tokenAddress => {
                    return new Promise(async resolve => {
                        const tokenBalanceEth = bigNumberify(state.tokens[tokenAddress].desiredBalanceEth)
                        const tokenPercentageFLOAT = (await dispatch('bnToDecimal', tokenBalanceEth) / await dispatch('bnToDecimal', groupDesiredBalanceEthBN)) * 100
                        commit('setTokenDesiredBalancePercentage', {
                            tokenAddress: tokenAddress,
                            number: tokenPercentageFLOAT
                        })
                        commit('setTokenCurrentBalancePercentage', {
                            tokenAddress: tokenAddress,
                            number: tokenPercentageFLOAT
                        })
                        commit("setTokenDesiredBalanceUSD", {
                            tokenAddress: tokenAddress,
                            number: await dispatch('BnEthToUSD', { bn: tokenBalanceEth }),
                        })
                        resolve()
                    })
                })
                ))
            })

        })
        )
    },
    async initEthPriceUSD({ commit, state }) {
        const ethPriceUSD = state.topTokensInfo.find(tokenInfo => {
            return tokenInfo.address === "0x0000000000000000000000000000000000000000"
        })
        if (ethPriceUSD.hasOwnProperty('price') && ethPriceUSD.price.hasOwnProperty('rate')) {
            commit('ethPriceUSD', { rate: ethPriceUSD.price.rate })
        }
    },
    // Actions
    async addGroup({ commit, dispatch }, { name }) {
        console.log('_' + Math.random().toString(36).substr(2, 9))
        let groupData = {
            id: '_' + Math.random().toString(36).substr(2, 9),
            name: name,
            tokens: [],
            currentBalanceEth: ethers.constants.Zero._hex,
            desiredBalanceEth: ethers.constants.Zero._hex,
            currentBalancePercentage: 0,
            desiredBalancePercentage: 0,
        }
        commit('addGroup', groupData)
        dispatch('setGroupDesiredBalancePercentage', {
            groupId: groupData.id, number: 0
        })
    },
    async setGroupBalances({ commit, state, dispatch }, { groupId, number }) {
        commit("setGroupDesiredBalancePercentage", {
            groupId: groupId,
            number: number,
        })
        if (number === 0) {
            commit("setGroupDesiredBalanceEth", {
                groupId: groupId,
                hex: ethers.constants.Zero,
            })
        }
        const totalValueEthDecimal = await dispatch('bnToDecimal', state.totalValueETH)
        const groupValueEthDecimal = (number / 100) * totalValueEthDecimal
        const groupValueEthBN = await dispatch('decimalToBn', groupValueEthDecimal)
        commit("setGroupDesiredBalanceEth", {
            groupId: groupId,
            hex: groupValueEthBN._hex,
        })
        commit("setGroupDesiredBalanceUSD", {
            groupId: groupId,
            number: await dispatch('hexEthToUSD', { hex: groupValueEthBN._hex }),
        })
        state.groups[groupId].tokens.forEach(tokenAddress => {
            dispatch('setTokenBalances', {
                groupId,
                tokenAddress,
                number: state.tokens[tokenAddress].desiredBalancePercentage
            })
        })
    },
    async setTokenBalances({ commit, state, dispatch }, { groupId, tokenAddress, number }) {
        commit("setTokenDesiredBalancePercentage", {
            groupId: groupId,
            tokenAddress: tokenAddress,
            number: number,
        })
        if (number === 0) {
            commit("setTokenDesiredBalanceEth", {
                tokenAddress: tokenAddress,
                hex: ethers.constants.Zero._hex,
            })
        }
        const totalGroupValueEthDecimal = await dispatch('bnToDecimal', state.groups[groupId].desiredBalanceEth)
        const tokenValueEthDecimal = (number / 100) * totalGroupValueEthDecimal
        const tokenValueEthBN = await dispatch('decimalToBn', tokenValueEthDecimal)
        commit("setTokenDesiredBalanceEth", {
            tokenAddress: tokenAddress,
            hex: tokenValueEthBN._hex,
        })
        commit("setTokenDesiredBalanceUSD", {
            tokenAddress: tokenAddress,
            number: await dispatch('hexEthToUSD', { hex: tokenValueEthBN._hex }),
        })
    },
    async setGroupDesiredBalancePercentage({ commit, state, dispatch }, { groupId, number }) {


        return new Promise(async resolve => {
            console.log("LOG:  Group calculation")
            // If only this group inn group, just set it to 100
            if (state.groupList.length === 1) {
                await dispatch("setGroupBalances", {
                    groupId: groupId,
                    number: 100,
                })
                resolve(100)
                return
            }
            const total = await dispatch('getAllGroupsDesiredBalancePercentage')
            const newTotal = total + (- state.groups[groupId].desiredBalancePercentage + number)
            const diff = total - newTotal
            console.log("LOG:  Total", total, "newTotal", newTotal, "diff", diff)
            if (diff > 0) {
                console.log("LOG:  Need to add", diff)
            } else {
                console.log("LOG:  Need to remove", diff)
            }

            const otherGroups = state.groupList.filter(otherGroupId => {
                // Dont factor inn this group
                if (otherGroupId === groupId) {
                    return false
                }
                // Only groups with a balance greather then 0 and less 100
                const otherGroupDesiredBalance = state.groups[otherGroupId].desiredBalancePercentage
                if (otherGroupDesiredBalance <= 0) {
                    return false
                }
                return true
            })
            console.log("LOG:  We have ", otherGroups.length, "groups to give/take value from")

            if (otherGroups.length === 0) {
                console.log("LOG:  Dont have any otherGroups to give/take value")
                console.log("LOG:  Must give diff back to group.")
                number = number + diff
            } else {
                let reminder = diff
                const maxRounds = 5
                let i = 0
                while (reminder !== 0 && i < maxRounds) {
                    i++
                    let diffSplit = diff / otherGroups.length
                    console.log("LOG:  Splitted gave/take", diffSplit)
                    reminder = await otherGroups.reduce((prev, otherGroupId) => {
                        return new Promise(async resolve => {
                            // What happens if diff split is more then reminder. Take care for negative and postive UNDER/OVER values
                            if ((reminder < 0 && diffSplit < reminder) || (reminder > 0 && diffSplit > reminder)) {
                                diffSplit = reminder
                            }
                            // What happens if reminder is 0
                            if (reminder === 0) {
                                resolve(0)
                                return
                            }
                            console.log("LOG:  START", state.groups[otherGroupId].name)
                            const balance = state.groups[otherGroupId].desiredBalancePercentage
                            let newBalance = balance + (diffSplit)
                            if (newBalance < 0) {
                                newBalance = 0
                                reminder = reminder + (balance)
                            } else if (newBalance > 100) {
                                newBalance = 100
                                reminder = reminder + (balance - 100)
                            } else {
                                reminder = reminder + (balance - newBalance)
                            }
                            console.log("LOG:  oldBalance", state.groups[otherGroupId].symbol, balance)
                            console.log("LOG:  newBalance", state.groups[otherGroupId], newBalance)
                            await dispatch("setGroupBalances", {
                                groupId: otherGroupId,
                                number: newBalance
                            })
                            console.log("LOG:  STOP", state.groups[otherGroupId].symbol)
                            resolve(reminder)
                            return
                        })
                    }, Promise.resolve(reminder)) // END reduce
                    console.log("LOG:  Reminder after reduce, before exiting while", reminder)
                    console.log("LOG:  #############")
                } // END WHILE
            } // END if else

            // Set this group to desired balance
            console.log("LOG:  Setting MAIN group =>", number)
            await dispatch("setGroupBalances", {
                groupId: groupId,
                number: number
            })
            resolve(number)
            return

        })
    },
    async setTokenDesiredBalancePercentage({ commit, state, dispatch }, { groupId, tokenAddress, number }) {
        return new Promise(async resolve => {
            console.log("LOG:  Token calculation")
            // If only this token inn group, just set it to 100
            if (state.groups[groupId].tokens.length === 1) {
                await dispatch("setTokenBalances", {
                    groupId: groupId,
                    tokenAddress: state.groups[groupId].tokens[0],
                    number: 100
                })
                resolve(100)
                return
            }
            const total = await dispatch('getAllGroupsAllTokensDesiredBalancePercentage', { groupId })
            const newTotal = total + (- state.tokens[tokenAddress].desiredBalancePercentage + number)
            const diff = total - newTotal
            console.log("LOG:  Total", total, "newTotal", newTotal, "diff", diff)
            if (diff > 0) {
                console.log("LOG:  Need to add", diff)
            } else {
                console.log("LOG:  Need to remove", diff)
            }

            const otherTokens = state.groups[groupId].tokens.filter(otherTokenAddress => {
                // Dont factor inn this token
                if (otherTokenAddress === tokenAddress) {
                    return false
                }
                // Only tokens with a balance greather then 0 and less 100
                const otherTokenDesiredBalance = state.tokens[otherTokenAddress].desiredBalancePercentage
                if (otherTokenDesiredBalance <= 0) {
                    return false
                }
                return true
            })
            console.log("LOG:  We have ", otherTokens.length, "tokens to give/take value from")

            if (otherTokens.length === 0) {
                console.log("LOG:  Dont have any otherTokens to give/take value")
                console.log("LOG:  Must give diff back to token.")
                number = number + diff
            } else {
                let reminder = diff
                const maxRounds = 5
                let i = 0
                while (reminder !== 0 && i < maxRounds) {
                    i++
                    let diffSplit = diff / otherTokens.length
                    console.log("LOG:  Splitted gave/take", diffSplit)
                    reminder = await otherTokens.reduce((prev, otherTokenAddress) => {
                        return new Promise(async resolve => {
                            // What happens if diff split is more then reminder. Take care for negative and postive UNDER/OVER values
                            if ((reminder < 0 && diffSplit < reminder) || (reminder > 0 && diffSplit > reminder)) {
                                diffSplit = reminder
                            }
                            // What happens if reminder is 0
                            if (reminder === 0) {
                                resolve(0)
                                return
                            }
                            console.log("LOG:  START", state.tokens[otherTokenAddress].symbol)
                            const balance = state.tokens[otherTokenAddress].desiredBalancePercentage
                            let newBalance = balance + (diffSplit)
                            if (newBalance < 0) {
                                newBalance = 0
                                reminder = reminder + (balance)
                            } else if (newBalance > 100) {
                                newBalance = 100
                                reminder = reminder + (balance - 100)
                            } else {
                                reminder = reminder + (balance - newBalance)
                            }
                            console.log("LOG:  oldBalance", state.tokens[otherTokenAddress].symbol, balance)
                            console.log("LOG:  newBalance", state.tokens[otherTokenAddress].symbol, newBalance)
                            await dispatch("setTokenBalances", {
                                tokenAddress: otherTokenAddress,
                                number: newBalance,
                                groupId: groupId,
                            })

                            console.log("LOG:  STOP", state.tokens[otherTokenAddress].symbol)
                            resolve(reminder)
                            return
                        })
                    }, Promise.resolve(reminder))
                    console.log("LOG:  Reminder after reduce, before exiting while", reminder)
                    console.log("LOG:  #############")
                }
            }



            // Set this token to desired balance
            console.log("LOG:  Setting MAIN token =>", number)
            await dispatch("setTokenBalances", {
                groupId: groupId,
                tokenAddress: tokenAddress,
                number: number,
            })
            resolve(number)
            return

        })
    },
    async addTokenToGroup({ commit, dispatch }, { groupId, tokenAddress }) {
        commit('setGroupToken', {
            groupId,
            tokenAddress,
        })
        dispatch('setTokenDesiredBalancePercentage', {
            groupId, tokenAddress, number: 10
        })
    },
    async removeTokenFromGroup({ commit, dispatch, state }, { groupId, tokenAddress }) {
        // Dont let user remove tokens they have a balance in, we need to know this so we can calculate rebalance. Just set it to 0
        if (bigNumberify(state.tokens[tokenAddress].currentBalanceEth).gt(ethers.constants.Zero)) {
            dispatch('setTokenDesiredBalancePercentage', {
                groupId,
                tokenAddress,
                number: 0
            })
        } else {
            commit("removeGroupToken", {
                tokenAddress: tokenAddress,
                groupId: groupId,
            })
            dispatch('setTokenDesiredBalancePercentage', {
                groupId, tokenAddress, number: 0
            })
        }
    },
    // Getter (actions)
    async getAllGroupsDesiredBalanceEth({ state }) {
        return state.groupList.reduce((prev, groupId) => {
            return prev.then(prevBN => {
                return new Promise(resolve => resolve(prevBN.add(bigNumberify(state.groups[groupId].desiredBalanceEth))))
            })
        }, Promise.resolve(ethers.constants.Zero))
    },
    async getAllGroupsDesiredBalancePercentage({ state }) {
        return state.groupList.reduce((prev, groupId) => {
            return prev.then(prevNmber => {
                return new Promise(resolve => resolve(prevNmber + state.groups[groupId].desiredBalancePercentage))
            })
        }, Promise.resolve(0))
    },
    async getAllGroupsAllTokensDesiredBalancePercentage({ state }, { groupId }) {
        return state.groups[groupId].tokens.reduce((prev, tokenAddress) => {
            return prev.then(prevNmber => {
                return new Promise(resolve => resolve(prevNmber + state.tokens[tokenAddress].desiredBalancePercentage))
            })
        }, Promise.resolve(0))
    },
    async getDesiredInTokens({ state, dispatch }, { tokenAddress }) {
        return new Promise(async (resolve, reject) => {
            if (tokenAddress === "0x0000000000000000000000000000000000000000") {
                resolve(await dispatch('bnToDecimal', state.tokens[tokenAddress].desiredBalanceEth))
                return
            }
            const web3Provider = new ethers.providers.Web3Provider(ethereum)
            const e2tTokenExchange = new ethers.Contract(state.tokens[tokenAddress].exchangeAddress, CONSTANTS.exchangeByteAbi, web3Provider.getSigner());
            const ethToTokenBN = await e2tTokenExchange.getEthToTokenInputPrice(state.tokens[tokenAddress].desiredBalanceEth)
            const ethToTokenDecimal = await dispatch('bnToDecimal', ethToTokenBN)
            resolve(ethToTokenDecimal)
        })
    },
    async getDesiredInUSD({ state, dispatch }, { tokenAddress }) {
        return new Promise(async (resolve, reject) => {
            const tokenEthBN = bigNumberify(state.tokens[tokenAddress].desiredBalanceEth)
            const tokenEthDecimal = await dispatch('bnToDecimal', tokenEthBN)
            const tokenPriceUSD = state.ethPriceUSD * tokenEthDecimal
            resolve(tokenPriceUSD)
        })
    },
    async hexEthToUSD({ state, dispatch }, { hex }) {
        return new Promise(async (resolve) => {
            const tokenEthBN = bigNumberify(hex)
            resolve(await dispatch('BnEthToUSD', { bn: tokenEthBN }))
        })
    },
    async BnEthToUSD({ state, dispatch }, { bn }) {
        return new Promise(async (resolve) => {
            const tokenEthDecimal = await dispatch('bnToDecimal', bn)
            const tokenPriceUSD = state.ethPriceUSD * tokenEthDecimal
            resolve(tokenPriceUSD)
        })
    },
    async getRandomColor() {
        //const colors = ['#F6F091', '#EC80B8', '#00DBA9', '#39A0ED', '#C2C6A7', '#7E7F9A', '#EB9486', '#8D5A97', '#473BF0', '#2B59C3', '#473BF0', '#F4D06F', '#60492C', '#EDF060', '#4A7C59', '#FCEC52', '#ADE25D', '#CFFFB3', '#1B2021']
        const colors = [
            '#FF0C00',
            '#EF3E36',
            '#F29D48',
            '#8CF248',
            '#48F273',
            '#48F2B9',
            '#48EFF2',
            '#4BAFF2',
            '#4B64F2',
            '#994BF2',
            '#E557F2',
            '#F257C6',
            '#F2576E',
        ]
        return colors[Math.floor(Math.random() * colors.length) + 1]
    },
    // Ethereum actions
    async getAllowance({ state, rootState, commit }, { tokenAddress }) {
        if (state.rebalanceContractAddress) {
            const web3Provider = new ethers.providers.Web3Provider(ethereum)
            if (state.tokens[tokenAddress].symbol === "ETH") {
                return state.tokens[tokenAddress].allowance
            }
            const tokenContract = new ethers.Contract(tokenAddress, TOKEN_JSON.abi, web3Provider.getSigner())
            const rebalanceContractAddress = state.rebalanceContractAddress[rootState.metamask.network]
            const allowance = await tokenContract.allowance(rootState.metamask.accounts[0], rebalanceContractAddress);
            commit('setAllowance', {
                tokenAddress,
                hex: allowance._hex
            })
            return allowance._hex
        } else {
            return ethers.constants.Zero
        }
    },
    async setAllowance({ state, rootState, commit, dispatch }, { tokenAddress }) {
        const web3Provider = new ethers.providers.Web3Provider(ethereum)
        const tokenContract = new ethers.Contract(tokenAddress, TOKEN_JSON.abi, web3Provider.getSigner())
        const rebalanceContractAddress = state.rebalanceContractAddress[rootState.metamask.network]

        const tx = await tokenContract.approve(rebalanceContractAddress, ethers.constants.MaxUint256);
        const etherscanTx = await dispatch('createEtherscanUrl', { tx: tx.hash })
        await tx.wait()
        commit('addMessage', {
            message: {
                title: "Transaction success",
                body: `Token ` + state.tokens[tokenAddress].symbol + " is unlocked!",
                variant: "success"
            }
        })
        const allowance = await tokenContract.allowance(rootState.metamask.accounts[0], rebalanceContractAddress);
        commit('setAllowance', {
            tokenAddress,
            hex: allowance._hex
        })
        return allowance
    },
    async calculateRebalance({ state, rootState, dispatch, commit }) {
        return new Promise(async resolve => {

            const web3Provider = new ethers.providers.Web3Provider(ethereum)
            const rebalanceContractAddress = state.rebalanceContractAddress[rootState.metamask.network]
            const rebalanceContract = new ethers.Contract(rebalanceContractAddress, REBALANCE_JSON.abi, web3Provider.getSigner())

            // We should only rebalance token within groups.
            // BuyMatrix and SellMatrix
            let buyMatrix = []
            let sellMatrix = []
            for (const groupIndex in state.groupList) {
                const groupId = state.groupList[groupIndex]
                for (const tokenIndex in state.groups[groupId].tokens) {
                    const tokenAddress = state.groups[groupId].tokens[tokenIndex]
                    const token = state.tokens[tokenAddress]
                    const deltaBalanceEth = bigNumberify(token.desiredBalanceEth).sub(bigNumberify(token.currentBalanceEth)) // Postive = BUY , Negative = SELL 
                    if (deltaBalanceEth.gt(ethers.constants.Zero)) {
                        console.log("LOG:  Buying", token.symbol, deltaBalanceEth)
                        buyMatrix.push({
                            tokenAddress: token.tokenAddress,
                            exchangeAddress: token.exchangeAddress,
                            amountEth: deltaBalanceEth._hex,
                            symbol: token.symbol,
                        })
                    } else if (deltaBalanceEth.lt(ethers.constants.Zero)) {
                        console.log("LOG:  Selling", token.symbol, deltaBalanceEth.abs())
                        sellMatrix.push({
                            tokenAddress: token.tokenAddress,
                            exchangeAddress: token.exchangeAddress,
                            amountEth: deltaBalanceEth.abs()._hex,
                            symbol: token.symbol,
                        })
                    } else {
                        // dont care about 0 sum
                    }
                }
            }
            console.log("LOG: buyMatrix", [...buyMatrix])
            console.log("LOG: sellMatrix", [...sellMatrix])

            // Swaps
            let swaps = []
            let i = 0
            const maxRounds = 5

            while (i < maxRounds) {
                i++

                if (!Array.isArray(buyMatrix) || !buyMatrix.length) {
                    console.log("Nothing more in Buy, rest", sellMatrix)
                    console.log("reminder", await dispatch('bnToDecimal', sellMatrix[0].amountEth))
                    break;
                }
                if (!Array.isArray(sellMatrix) || !sellMatrix.length) {
                    console.log("Nothing more in Sell, rest", buyMatrix)
                    console.log("reminder", await dispatch('bnToDecimal', buyMatrix[0].amountEth))
                    break;
                }

                // If BUY is bigger then SELL, create a swap where you BUY for everything you can sell
                //  remove how much you can from BUY,  ZERO out SELL by shifting it out.
                let buyAmountEth = bigNumberify(buyMatrix[0].amountEth)
                let sellAmountEth = bigNumberify(sellMatrix[0].amountEth)
                if (buyAmountEth.gt(sellAmountEth)) {
                    console.log("Buy")
                    console.log("buyMatrix[0].amountEth", buyAmountEth)
                    console.log("sellMatrix[0].amountEth", sellAmountEth)
                    swaps.push({
                        buy: buyMatrix[0],
                        sell: sellMatrix[0],
                        amountEth: sellAmountEth._hex
                    })
                    buyMatrix[0].amountEth = buyAmountEth.sub(sellAmountEth)._hex
                    sellMatrix.shift()
                } else {
                    // If SELL bigger then BUY, create a swap where you SELL for everything you can BUY
                    // remove how much you can from SELL,  ZERO out BUY by shifting it out.
                    swaps.push({
                        buy: buyMatrix[0],
                        sell: sellMatrix[0],
                        amountEth: buyAmountEth._hex
                    })
                    sellMatrix[0].amountEth = sellAmountEth.sub(buyAmountEth)._hex
                    buyMatrix.shift()
                    // reminderBN = buyMatrix[0].buy.add(sellMatrix[0].amountEth)
                }
            }
            console.log("SWAPS", { ...swaps[0] })
            console.log("---")

            // Generate TX

            let e2tTo = []
            let e2tAmount = []

            let t2tFromToken = []
            let t2tFromExchange = []
            let t2tAmount = []
            let t2tToToken = []

            let t2eFromToken = []
            let t2eFromExchange = []
            let t2eAmount = []


            await Promise.all(swaps.map(swap => {
                return new Promise(async resolve => {
                    if (swap.sell.symbol === "ETH") {
                        // address[] memory e2tTo, uint[] memory e2tAmount, uint e2tLen,
                        const e2tTokenExchange = new ethers.Contract(swap.buy.exchangeAddress, CONSTANTS.exchangeByteAbi, web3Provider.getSigner());
                        const e2tamountToken = await e2tTokenExchange.getEthToTokenInputPrice(swap.amountEth)
                        console.log(ethers.utils.parseBytes32String(await e2tTokenExchange.symbol()));

                        console.log("Swapping ", await dispatch('bnToDecimal', swap.amountEth), "ETH for ", await dispatch('bnToDecimal', e2tamountToken), swap.buy.symbol)
                        e2tTo.push(swap.buy.exchangeAddress)
                        e2tAmount.push(swap.amountEth)
                        console.log(swap.sell.symbol, "=>", swap.buy.symbol)
                    }

                    if (swap.buy.symbol !== "ETH" && swap.sell.symbol !== "ETH") {
                        // address[] memory t2tFromToken, address[] memory t2tFromExchange, uint[] memory t2tAmount, address[] memory t2tToToken, uint t2tLen,
                        const t2tTokenExchange = new ethers.Contract(swap.sell.exchangeAddress, CONSTANTS.exchangeByteAbi, web3Provider.getSigner());
                        const t2tamountToken = await t2tTokenExchange.getEthToTokenInputPrice(swap.amountEth)
                        console.log("Swapping ", await dispatch('bnToDecimal', swap.amountEth), swap.sell.symbol, " for ", await dispatch('bnToDecimal', t2tamountToken), swap.buy.symbol)
                        t2tFromToken.push(swap.sell.tokenAddress)
                        t2tFromExchange.push(swap.sell.exchangeAddress)
                        t2tAmount.push(t2tamountToken)
                        t2tToToken.push(swap.buy.tokenAddress)
                        console.log(swap.sell.symbol, "=>", swap.buy.symbol)
                    }

                    if (swap.buy.symbol === "ETH") {
                        // address[] memory t2eFromToken, address[] memory t2eFromExchange, uint[] memory t2eAmount, uint t2eLen
                        const t2eTokenExchange = new ethers.Contract(swap.sell.exchangeAddress, CONSTANTS.exchangeByteAbi, web3Provider.getSigner());
                        const t2eamountToken = await t2eTokenExchange.getEthToTokenInputPrice(swap.amountEth)
                        console.log("Swapping ", await dispatch('bnToDecimal', t2eamountToken), swap.sell.symbol, " for ", await dispatch('bnToDecimal', swap.amountEth), swap.buy.symbol)
                        t2eFromToken.push(swap.sell.tokenAddress)
                        t2eFromExchange.push(swap.sell.exchangeAddress)
                        t2eAmount.push(t2eamountToken)
                        console.log(swap.sell.symbol, "=>", swap.buy.symbol)
                    }
                    resolve()
                })
            }))
            console.log("e2tTo", e2tTo)
            console.log("e2tAmount", e2tAmount)
            console.log("e2tTo.length", e2tTo.length)
            console.log("---")
            console.log("t2tFromToken", t2tFromToken)
            console.log("t2tFromExchange", t2tFromExchange)
            console.log("t2tAmount", t2tAmount)
            console.log("t2tToToken", t2tToToken)
            console.log("t2tFromToken.length", t2tFromToken.length)
            console.log("---")
            console.log("t2eFromToken", t2eFromToken)
            console.log("t2eFromExchange", t2eFromExchange)
            console.log("t2eAmount", t2eAmount)
            console.log("t2eFromToken.length", t2eFromToken.length)
            console.log("-----")
            console.log("HELE SOM SENDES INN")
            console.log("Value: ", e2tTo)
            const totalEthAmount = e2tAmount.reduce((prev, amount) => {
                console.log(prev, amount)
                return prev.add(amount)
            }, bigNumberify(ethers.constants.Zero))
            console.log("tot", await dispatch('bnToDecimal', totalEthAmount))
            console.log(e2tTo, e2tAmount, e2tTo.length, t2tFromToken, t2tFromExchange, t2tAmount, t2tToToken, t2tFromToken.length, t2eFromToken, t2eFromExchange, t2eAmount, t2eFromToken.length)

            const tx_overrides = (process.env.NODE_ENV === "production") ?
                {
                    value: totalEthAmount
                }
                :
                {
                    value: totalEthAmount,
                    gasLimit: 8000000,
                }

            const tx = await rebalanceContract.swap(e2tTo, e2tAmount, e2tTo.length, t2tFromToken, t2tFromExchange, t2tAmount, t2tToToken, t2tFromToken.length, t2eFromToken, t2eFromExchange, t2eAmount, t2eFromToken.length, tx_overrides)
            dispatch('createEtherscanUrl', { tx: tx.hash })
            await tx.wait()
            commit('addMessage', {
                message: {
                    title: "Transaction success👍",
                    body: `Your rebalance is complete!`,
                    variant: "success"
                }
            })
            /*         
            address[] memory e2tTo, uint[] memory e2tAmount, uint e2tLen,
            address[] memory t2tFromToken, address[] memory t2tFromExchange, uint[] memory t2tAmount, address[] memory t2tToToken, uint t2tLen,
            address[] memory t2eFromToken, address[] memory t2eFromExchange, uint[] memory t2eAmount, uint t2eLen
             */
            resolve()
        })
    },
    async unlockAll({ state, dispatch }) {
        for (const groupIndex in state.groupList) {
            const groupId = state.groupList[groupIndex]
            for (const tokenIndex in state.groups[groupId].tokens) {
                const tokenAddress = state.groups[groupId].tokens[tokenIndex]
                const token = state.tokens[tokenAddress]
                // Check if unlocked is needed
                const allowanceHex = await dispatch('getAllowance', {
                    tokenAddress
                })
                const allowance = bigNumberify(allowanceHex)
                const desired = bigNumberify(token.desiredBalanceEth)
                const current = bigNumberify(token.currentBalanceEth)
                const selling = desired.lt(current)
                const allowanceForAmountSelling = allowance.lt(desired)
                if (selling && allowanceForAmountSelling) {
                    await dispatch('setAllowance', {
                        tokenAddress
                    })
                }
            }
        }
    },
    getAddressInfo({ commit }, accountToCheck) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await get(`https://api.ethplorer.io/getAddressInfo/${accountToCheck}?apiKey=${ETHPLORER_API_KEY}`)
                resolve(res)
            } catch (error) {
                commit('tryAgain', true)
                reject("Error getAddressInfo from Ethplorer API")
            }
        })
    },
    getTokenInfo({ commit }, tokenAddress) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await get(`https://api.ethplorer.io/getTokenInfo/${tokenAddress}?apiKey=${ETHPLORER_API_KEY}`)
                resolve(res)
            } catch (error) {
                commit('tryAgain', true)
                reject("Error getTokenInfo from Ethplorer API")
            }
        })
    },
    getTopTokenInfo({ commit, state }, limit) {
        return new Promise(async (resolve, reject) => {
            try {
                if (state.topTokensInfo.length < 1) {
                    const res = await get(`https://api.ethplorer.io/getTop?apiKey=freekey&criteria=cap&limit=${limit}`)
                    commit("setTopTokenInfo", { tokens: res.data.tokens })
                }
                resolve(state.topTokensInfo)
            } catch (error) {
                commit('tryAgain', true)
                reject("Error getTopTokenInfo from Ethplorer API")
            }
        })
    },
    // Utils
    async fromTokenAddressesGetDesiredBalanceEth({ state }, tokenAddresses) {
        return tokenAddresses.reduce(async (prev, tokenAddress) => {
            return prev.then(BN => {
                return new Promise(resolve => resolve(BN.add(bigNumberify(state.tokens[tokenAddress].desiredBalanceEth))))
            })
        }, Promise.resolve(ethers.constants.Zero))
    },
    bnToDecimal({ commit }, BN) {
        return parseFloat(ethers.utils.formatEther(BN))
    },
    decimalToBn({ commit }, decimal) {
        return ethers.utils.parseEther(decimal.toString())
    },
    createEtherscanUrl({ rootState, commit }, { tx }) {
        const networkToSubdomain = {
            1: "",
            4: "rinkeby."
        }
        const subdomain = networkToSubdomain[rootState.metamask.network] ? networkToSubdomain[rootState.metamask.network] : ""
        const url = `https://${subdomain}etherscan.io/tx/${tx}`
        commit('addMessage', {
            message: {
                title: "Waiting for transaction.",
                body: `See your transaction on Etherscan`,
                link: url,
                variant: "primary"
            }
        })
        return Promise.resolve(url)
    },
}