import { bigNumberify } from 'ethers/utils';

export const getters = {
    tokens: (state) => state.tokenList.map(tokenId => state.tokens[tokenId]),
    token: (state) => (tokenId) => state.tokens[tokenId],
    tokenAddressInGroup: (state) => (tokenAddress) => state.groupList.find(groupId => {
        return state.groups[groupId].tokens.find(otherTokenAddress => {
            return tokenAddress === otherTokenAddress
        })
    }),
    desiredBalanceEth: (state) => (groupId) => state.groups[groupId].desiredBalanceEth,
    groups: (state) => state.groupList.map(groupId => state.groups[groupId]),
    groupTokens: (state) => (groupId) => state.tokenList.filter(tokenId => {
        return state.groups[groupId].tokens.find(groupTokenId => tokenId === groupTokenId)
    }).map(tokenId => {
        return state.tokens[tokenId]
    }),
    readyForRebalance: (state) => state.groupList.find(groupId => {
        return state.groups[groupId].tokens.find(tokenAddress => {
            const token = state.tokens[tokenAddress]
            const desired = bigNumberify(token.desiredBalanceEth)
            const current = bigNumberify(token.currentBalanceEth)
            const allowance = bigNumberify(token.allowance)
            const selling = desired.lt(current)
            const allowanceForAmountSelling = allowance.lt(desired)
            if (selling && allowanceForAmountSelling) {
                return true
            }
            return false
        })
    }) === undefined,
}